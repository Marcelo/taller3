# Taller3

Este proyecto contiene tres archivos:


1. El primero, hace uso de las librerias GLU y GLUT para crear figuras y rotarlas.

Contiene las carpetas:
- build
- nbproject
- src
- test

![imagen1](https://framagit.org/Marcelo/taller3/-/raw/master/Img/GLU.png)

2. El segundo, usa las transformadas para girar, rotar y mover una figura.

Contiene las carpetas:
- build
- nbproject
- src
- test

![imagen2](https://framagit.org/Marcelo/taller3/-/raw/master/Img/Transformadas.png)

3. Y por último la consulta de glPushmatrix(), glPopMatrix().

Contiene un archivo pdf con la consulta.