package org.jmtipane;

import com.sun.opengl.util.Animator;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;



/**
 * Taller_3_Traslación,Rotación
 * @author Marcelo Tipán
 * Programa que rota y traslada un cuadrado, mediante el teclado o el mouse
 * 18/07/2020
 */
public class MouseKeyControl extends JFrame implements GLEventListener, KeyListener, MouseListener, MouseMotionListener {
    
    //variables de Opengl
    static GL gl;
    static GLU glu;
    //variables de traslación y rotación
    static float angx=0, angy=0, angz=0, cenx=0, ceny=0;
    static  float tx=0,ty=0,sx=1,sy=1,rx=0,ry=0, rz=0;
    
    /**
     * Crea una ventana y la muestra
     * @param args 
     */
    public static void main(String[] args) {
        Frame frame = new Frame("Control de Mouse&Teclado");
        GLCanvas canvas = new GLCanvas();
        
        canvas.addGLEventListener(new MouseKeyControl());
        frame.add(canvas);
        frame.setSize(700, 700);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // hace visible la pantalla, la centra y comienza la animación
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
        
        //llama a las funciones que controlan el mouse y el teclado
        frame.addKeyListener(new MouseKeyControl());
        canvas.addMouseListener(new MouseKeyControl());
        canvas.addMouseMotionListener(new MouseKeyControl());
    }

    /**
     * Inicializa variables y color
     * @param drawable 
     */
    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
        
    }

    /**
     * define la forma, perspectiva y dimensiones de la pantalla
     * @param drawable
     * @param x
     * @param y
     * @param width
     * @param height 
     */
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        
        //define los limites y produndidad de la ventana
//        glu.gluOrtho2D(-350, 350, +350, -350);
        gl.glOrtho(-350, 350, 350, -350, -200, 200);
        
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    /**
     * Hace uso de las variables gl y glu para depinir color, formas y dibujar un cuadrado
     * Hace uso de funciones gl para rotar y trasladar a la figura
     * @param drawable 
     */
    public void display(GLAutoDrawable drawable) {
        glu = new GLU();
        gl = drawable.getGL();
        
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();
        
        //Esta parte traslada y rota al cuerpo
        gl.glTranslatef(tx, ty, 0.0f);
        gl.glRotated(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        
        //define un punto central en el cuerpo
        cenx= tx+350;
        ceny= ty+350;
        
        // Dibuja un cuadrado
        gl.glBegin(GL.GL_QUADS);
            gl.glColor3f(1.0f, 0.0f, 0.0f);    // Set the current drawing color to light blue
            gl.glVertex3f(-100.0f, 100.0f, 0.0f);  // Top Left
            gl.glVertex3f(100.0f, 100.0f, 0.0f);   // Top Right
            gl.glVertex3f(100.0f, -100.0f, 0.0f);  // Bottom Right
            gl.glVertex3f(-100.0f, -100.0f, 0.0f); // Bottom Left
        gl.glEnd();
        
        gl.glBegin(GL.GL_LINES);
            gl.glColor3f(1.0f, 1.0f, 1.0f);
            gl.glVertex3f(-100.0f, 101.0f, 0.0f);  // Top Left
            gl.glVertex3f(100.0f, 101.0f, 0.0f);   // Top Right
            
            gl.glVertex3f(100.0f, -101.0f, 0.0f);  // Bottom Right
            gl.glVertex3f(-100.0f, -101.0f, 0.0f); // Bottom Left
        // Done Drawing The Quad
        gl.glEnd();

        // Flush all drawing operations to the graphics card
        gl.glFlush();
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }

    
    public void keyTyped(KeyEvent e) {
    }

    /**
     * controla que hacen las teclas al ser pulsadas
     * @param e 
     */
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
        tx+=10f;
    }
        if(e.getKeyCode() == KeyEvent.VK_LEFT){
        tx-=10f;
        }
        if(e.getKeyCode() == KeyEvent.VK_UP){
        ty-=10f;
        }
        if(e.getKeyCode() == KeyEvent.VK_DOWN){
        ty+=10f;}
        
        if(e.getKeyCode() == KeyEvent.VK_J){
        rx=1f;
        angx=angx+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_L){
        rx=1f;
        angx=angx-5;
        }
        if(e.getKeyCode() == KeyEvent.VK_I){
        ry=1f;
        angy=angy+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_K){
        ry=1f;
        angy=angy-5;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_N){
        rz=1f;
        angz=angz+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_M){
        rz=1f;
        angz=angz-5;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_I){
        ry=1f;
        angy=angy+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_K){
        ry=1f;
        angy=angy-5;
        }
        if(e.getKeyCode() == KeyEvent.VK_R){
        angx=0; angy=0; angz=0; cenx=0; ceny=0;
        tx=0;ty=0; sx=1; sy=1; rx=0; ry=0; rz=0;
        }
    }

    public void keyReleased(KeyEvent e) {
    }

    /**
     * controla que sucede al pulsar los botones del mouse
     * @param e 
     */
    public void mouseClicked(MouseEvent e) {
        if(e.getButton() == 2){
            if(e.getX()>cenx){
                tx+=10f;
                cenx+=10f;
            }
            if(e.getX()<cenx){
                tx-=10f;
                cenx-=10f;
            }
            if(e.getY() > ceny){
                ty+=10f;
                ceny=ceny+10f;
                System.out.println(e.getY());
                System.out.println("cen"+ceny);
            }
            if(e.getY() < ceny){
                ty-=10f;
                ceny=ceny-10f;
                System.out.println(e.getY());
                System.out.println("cen"+ceny);
            }
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    //controla el movimiento del mouse
    public void mouseMoved(MouseEvent e) {
        while(e.getX()>cenx){
                tx+=10f;
                cenx+=10f;
            }
            while(e.getX()<cenx){
                tx-=10f;
                cenx-=10f;
            }
            while(e.getY() > ceny){
                ty+=10f;
                ceny=ceny+10f;
                System.out.println(e.getY());
                System.out.println("cen"+ceny);
            }
            while(e.getY() < ceny){
                ty-=10f;
                ceny=ceny-10f;
                System.out.println(e.getY());
                System.out.println("cen"+ceny);
            }
    }
}

